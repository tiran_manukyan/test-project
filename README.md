# Run Guide
## 1. Run main class com.github.tiran.Application


# API Guide
## 1.1 Upload multiple files

#### Request-Example:
```endpoint
POST {host}:8080/files
```

*Content-Type:* __multipart/form-data__ <br>
*Parameter name:* __files__

#### Success-Response: (HTTP/1.1 200 OK)
```text
"done"
```

## 1.2 Upload base64 file

**Parameter**

Field | Type | Description | 
--- | --- |--- |
type | String | File source type <br> Required value: base64

#### Request-Example:
```endpoint
POST {host}:8080/files?type=base64
```
### &nbsp;
```json
{
    "base64": "Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=",
    "outputFileName": "fileName"
}
```

#### Success-Response: (HTTP/1.1 200 OK)
```text
"done"
```

## 1.3 Upload file from url

**Parameter**

Field | Type | Description | 
--- | --- |--- |
type | String | File source type <br> Required value: url

#### Request-Example:
```endpoint
POST {host}:8080/files?type=url
```

```json
{
    "url": "https://pbs.twimg.com/media/EUFL_X3UwAAABh-.jpg",
    "outputFileName": "fileName"
}
```

#### Success-Response: (HTTP/1.1 200 OK)
```text
"done"
```

## 1.4 Create Image Thumbnail

#### Request-Example:
```endpoint
POST {host}:8080/files/images/thumbnail
```

*Content-Type:* __multipart/form-data__ <br>
*Parameter name:* __file__

#### Success-Response: (HTTP/1.1 200 OK)

*Content-Type:* __image/*__ <br>