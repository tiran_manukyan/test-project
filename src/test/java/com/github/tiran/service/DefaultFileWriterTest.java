package com.github.tiran.service;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URL;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.*;

public class DefaultFileWriterTest {

    private final DefaultFileWriter fileWriter = new DefaultFileWriter();

    private static final String rootDirectory = DefaultFileWriter.rootDirectory;

    @Test
    void writeMultipart() throws IOException {
        byte[] bytes = new byte[]{1, 2, 3, 7, 8, 9, 15, 16};
        String myFileName = "myFile";
        MultipartFile multipartFile = new MockMultipartFile("file", myFileName, "custom", bytes);

        fileWriter.write(multipartFile);

        File file = new File(rootDirectory + myFileName);

        assertTrue(file.exists());
        assertEquals(bytes.length, file.length());

        byte[] actualBytes = new byte[bytes.length];
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            int read = fileInputStream.read(actualBytes);
            assertEquals(bytes.length, read);
        }

        assertArrayEquals(bytes, actualBytes);

        file.delete();
    }

    @Test
    void writeBase64() throws IOException {
        byte[] bytes = new byte[]{1, 2, 3, 7, 8, 9, 15, 16};
        String myFileName = "myFile";
        String base64 = new String(Base64.getEncoder().encode(bytes));

        fileWriter.write(base64, myFileName);

        File file = new File(rootDirectory + myFileName);

        assertTrue(file.exists());
        assertEquals(bytes.length, file.length());

        byte[] actualBytes = new byte[bytes.length];
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            int read = fileInputStream.read(actualBytes);
            assertEquals(bytes.length, read);
        }

        assertArrayEquals(bytes, actualBytes);

        file.delete();
    }

    @Test
    void downloadAndWrite() throws IOException {
        byte[] bytes = new byte[]{1, 2, 3, 7, 8, 9, 15, 16};
        String myFileName = "myDuke";
        String outputFileName = "yourDuke";

        File myFile = new File(myFileName);
        try (FileOutputStream inputStream = new FileOutputStream(myFile)) {
            inputStream.write(bytes);
        }

        URL url = new URL("file", "", myFileName);

        fileWriter.downloadAndWrite(url.toString(), outputFileName);

        File file = new File(rootDirectory + outputFileName);

        assertTrue(file.exists());
        assertEquals(bytes.length, file.length());

        byte[] actualBytes = new byte[bytes.length];
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            int read = fileInputStream.read(actualBytes);
            assertEquals(bytes.length, read);
        }

        assertArrayEquals(bytes, actualBytes);

        myFile.delete();
        file.delete();
    }

    @Test
    void createImageThumbnail() throws IOException {
        String outputFileName = "outputDuke.png";

        InputStream fileResourceStream = DefaultFileWriterTest.class.getResourceAsStream("/duke.png");

        fileWriter.createImageThumbnail(fileResourceStream, new FileOutputStream(outputFileName), "png");

        File file = new File(outputFileName);

        assertTrue(file.exists());

        int outputFileLength = (int) file.length();

        byte[] outputFileBytes = new byte[outputFileLength];
        try (InputStream fileInputStream = new FileInputStream(outputFileName)) {
            int read = fileInputStream.read(outputFileBytes);
            assertEquals(outputFileLength, read);
        }

        byte[] existingThumbnailBytes = new byte[4440];
        try (InputStream fileInputStream = DefaultFileWriterTest.class.getResourceAsStream("/thumbnail.png")) {
            int read = fileInputStream.read(existingThumbnailBytes);
            assertEquals(existingThumbnailBytes.length, read);
        }

        assertArrayEquals(existingThumbnailBytes, outputFileBytes);

        file.delete();
    }
}
