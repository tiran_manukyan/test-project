package com.github.tiran.endpoint;

import com.github.tiran.service.FileWriter;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class ControllerConfig {

    @Bean
    public FilesController filesController() {
        return new FilesController(fileWriter());
    }

    @Bean
    public FileWriter fileWriter() {
        return Mockito.mock(FileWriter.class);
    }
}
