package com.github.tiran.endpoint;

import com.github.tiran.service.FileWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ControllerConfig.class})
public class FilesControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private FileWriter fileWriter;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        Mockito.clearInvocations(fileWriter);
    }

    @Test
    public void uploadFiles() throws Exception {
        byte[] bytes1 = new byte[]{1, 2, 3, 7, 8, 9, 15, 16};
        byte[] bytes2 = new byte[]{2, 3, 4, 9, 8, 7, 17, 19};
        mockMvc.perform(MockMvcRequestBuilders.multipart("/files")
                .file("files", bytes1)
                .file("files", bytes2)
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE))
                .andExpect(status().is(200))
                .andExpect(content().string("done"));

        ArgumentCaptor<MultipartFile> argumentCaptor = ArgumentCaptor.forClass(MultipartFile.class);
        Mockito.verify(fileWriter, Mockito.times(2)).write(argumentCaptor.capture());

        MultipartFile value1 = argumentCaptor.getAllValues().get(0);
        MultipartFile value2 = argumentCaptor.getAllValues().get(1);
        assertArrayEquals(bytes1, value1.getBytes());
        assertArrayEquals(bytes2, value2.getBytes());
    }

    @Test
    void uploadBase64() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/files")
                .queryParam("type", "base64")
                .content("{\n" +
                        "  \"base64\": \"base65\",\n" +
                        "  \"outputFileName\": \"myFileName\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().string("done"));

        Mockito.verify(fileWriter, Mockito.times(1)).write(eq("base65"), eq("myFileName"));
    }

    @Test
    void uploadFileByUrl() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/files?type=url")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"url\": \"http://lol.com/lmfao.jpg\",\n" +
                        "  \"outputFileName\": \"myFileName\"\n" +
                        "}"))
                .andExpect(status().is(200))
                .andExpect(content().string("done"));

        Mockito.verify(fileWriter, Mockito.times(1)).downloadAndWrite(eq("http://lol.com/lmfao.jpg"), eq("myFileName"));
    }

    @Test
    void createImageThumbnail() throws Exception {
        byte[] bytes = new byte[]{1, 2, 3, 7, 8, 9, 15, 16};
        mockMvc.perform(MockMvcRequestBuilders.multipart("/files/images/thumbnail")
                .file(new MockMultipartFile("file", "my-file", "image/jpeg", bytes))
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE))
                .andExpect(status().is(200));

        Mockito.verify(fileWriter).createImageThumbnail(any(), any(), eq("jpeg"));
    }
}