package com.github.tiran.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UrlFileRequestDto {

    private final String url;

    private final String outputFileName;

    @JsonCreator
    public UrlFileRequestDto(@JsonProperty("url") String url, @JsonProperty("outputFileName") String outputFileName) {
        this.url = url;
        this.outputFileName = outputFileName;
    }

    public String getUrl() {
        return url;
    }

    public String getOutputFileName() {
        return outputFileName;
    }
}
