package com.github.tiran.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Base64FileRequestDto {

    private final String base64;

    private final String outputFileName;

    @JsonCreator
    public Base64FileRequestDto(@JsonProperty("base64") String base64, @JsonProperty("outputFileName") String outputFileName) {
        this.base64 = base64;
        this.outputFileName = outputFileName;
    }

    public String getBase64() {
        return base64;
    }

    public String getOutputFileName() {
        return outputFileName;
    }
}
