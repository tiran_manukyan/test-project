package com.github.tiran.endpoint;

import com.github.tiran.dto.Base64FileRequestDto;
import com.github.tiran.dto.UrlFileRequestDto;
import com.github.tiran.service.FileWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/files")
public class FilesController {

    private final FileWriter fileWriter;

    @Autowired
    public FilesController(FileWriter fileWriter) {
        this.fileWriter = fileWriter;
    }

    @PostMapping()
    public String uploadFiles(@RequestParam("files") List<MultipartFile> files) {
        for (MultipartFile file : files) {
            fileWriter.write(file);
        }
        return "done";
    }

    @PostMapping(params = "type=base64")
    public String uploadBase64(@RequestBody Base64FileRequestDto requestDto) {
        fileWriter.write(requestDto.getBase64(), requestDto.getOutputFileName());
        return "done";
    }

    @PostMapping(params = "type=url")
    public String uploadFileByUrl(@RequestBody UrlFileRequestDto requestDto) {
        fileWriter.downloadAndWrite(requestDto.getUrl(), requestDto.getOutputFileName());
        return "done";
    }

    @PostMapping(value = "/images/thumbnail")
    public void createImageThumbnail(@RequestParam("file") MultipartFile file, HttpServletResponse response) throws IOException {
        ServletOutputStream outputStream = response.getOutputStream();
        String fileFormat = getImageFormatFromContentType(file.getContentType());

        fileWriter.createImageThumbnail(file.getInputStream(), outputStream, fileFormat);
    }

    private static String getImageFormatFromContentType(String contentType) {
        if (contentType == null) {
            return null;
        }
        String[] parts = contentType.split("/");
        if (parts.length > 1) {
            return parts[1];
        }

        return null;
    }
}