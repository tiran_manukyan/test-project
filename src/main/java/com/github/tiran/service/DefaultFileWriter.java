package com.github.tiran.service;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

@Component
public class DefaultFileWriter implements FileWriter {

    public static final String rootDirectory = "files/";

    @Override
    public void write(MultipartFile multipart) {
        createDirectoryIfNotExists();

        try {
            Path filepath = Paths.get(rootDirectory, multipart.getOriginalFilename());
            multipart.transferTo(filepath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void write(String base64, String outputFileName) {
        createDirectoryIfNotExists();

        byte[] bytes = Base64.getDecoder().decode(base64);
        try (FileOutputStream outputStream = new FileOutputStream(rootDirectory + outputFileName)) {
            outputStream.write(bytes);
        } catch (IOException ioe) {
            throw new RuntimeException("Exception while writing the file ", ioe);
        }
    }

    @Override
    public void downloadAndWrite(String fileUrl, String outputFileName) {
        createDirectoryIfNotExists();

        String[] parts = fileUrl.split("\\.");
        String extension = "";
        if (parts.length > 1) {
            extension = "." + parts[parts.length - 1];
        }

        URL url;
        try {
            url = new URL(fileUrl);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

        String fileName = outputFileName + extension;
        try (ReadableByteChannel rbc = Channels.newChannel(url.openStream());
             FileOutputStream fos = new FileOutputStream(rootDirectory + fileName)) {
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void createImageThumbnail(InputStream imageStream, OutputStream outputStream, String fileFormat) {
        createDirectoryIfNotExists();

        try (imageStream; outputStream) {
            BufferedImage img = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);

            BufferedImage bufferedImage = ImageIO.read(imageStream);

            if (bufferedImage == null) {
                throw new IllegalArgumentException("Invalid image format");
            }

            img.createGraphics().drawImage(bufferedImage
                    .getScaledInstance(100, 100, Image.SCALE_SMOOTH), 0, 0, null);

            ImageIO.write(img, fileFormat == null ? "jpg" : fileFormat, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void createDirectoryIfNotExists() {
        File directory = new File(rootDirectory);

        if (!directory.exists()) {
            directory.mkdir();
        }
    }
}
