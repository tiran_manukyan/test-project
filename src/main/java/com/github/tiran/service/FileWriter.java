package com.github.tiran.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.io.OutputStream;

public interface FileWriter {

    void write(MultipartFile multipart);

    void write(String base64, String outputFileName);

    void downloadAndWrite(String fileUrl, String outputFileName);

    void createImageThumbnail(InputStream imageStream, OutputStream outputStream, String fileFormat);
}
